# Date Dammit

## Usage:
Date Dammit sits on the `window`. So just add times in ISO format.

You can add a callback or a dom or an object (for Vue?) to update.

```javascript
  var o = {}
  DateDammit.add("2017-06-28T12:10:11.074Z", {
    id: 'time1',
    cb: (newTime) => {
      console.log(newTime)
    },
    obj: o,
    dom: '#time1'
  });
  DateDammit.add("2016-05-28T12:10:11.074Z", {
    obj: o,
  })
  setInterval(()=>{
    console.log(o);
  },1000)
```

## API
* `DateDammit.template`
 * Default template: `'{{count}} {{units}} ago'`;
 * Tells Date Dammit how to output your sentence on a global level. Must add mustaches for `count` and `units`. This will output something like `2 months ago`.
* `DateDammit.updateInterval`
 * How often Date Dammit will update.
* `DateDammit.add(ISOString, options)`
 * `options{}`
   * `cb`: Give the date a callback.
   * `obj`: Give the date a object to update. Will update with property of `id` or `timeAgo` if no id is specified.
   * `dom`: Give the date a dom object to update. Will use `innerText` to update.
   * `id`: Give the date an id as a property for the obj to populate, for Vueish stuff.
   * `template`: Give the date a template for just this date.
     * e.g `'{{count}} super crazy {{units}} agoz'`

