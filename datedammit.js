(function(w) {
  w.DateDammit = w.DateDammit || {};
  const dd = DateDammit;
  dd.template = '{{count}} {{units}} ago';
  dd.timeUnits = [['second', 'seconds'], ['minute', 'minutes'], ['hour', 'hours'], ['day', 'days'], ['month', 'months'], ['year', 'years']];
  dd.updateInterval = 2000;
  dd.toSeconds = (m) => Math.round((m / 1000) % 60);
  dd.toMinutes = (m) => Math.round((m / (1000*60)) % 60);
  dd.toHours = (m) => Math.round((m / (1000*60*60)) % 24);
  dd.toDays = (m) => Math.round((m / 86400000));
  dd.toMonths = (m) => Math.round(m / 2628000000);
  dd.toYears = (m) => Math.round(m / 31540000000);
  
  dd.toTime = (m) => {
    return {
      seconds: dd.toSeconds(m),
      minutes: dd.toMinutes(m),
      hours: dd.toHours(m),
      days: dd.toDays(m),
      months: dd.toMonths(m),
      years: dd.toYears(m)
    }
  }

  dd.pluralize = function(count, word) {
    if(count === 1) return word[0];
    return word[1];
  }
  
  dd.ago = function(obj, overrideTemplate) {
    let message = '';
    let template = overrideTemplate || dd.template;
    dd.timeUnits.forEach((tu) => {
      const count = obj[tu[1]];
      if(count > 0) {
        message = template
          .replace('{{count}}', count)
          .replace('{{units}}', dd.pluralize(count, tu));
      }
    });
    if(message === '') {
      return 'in the future';
    }
    return message;
  }
  
  dd.timerCallback = function() {
    const n = new Date();
    dd.timesToCheck.forEach((d) => {
      let time = dd.ago(dd.toTime(n-d.date), d.options.template);
      if(d.options.dom){
        document.querySelector(d.options.dom).innerText = time;
      }
      if(d.options.cb) {
        d.options.cb(time);
      }
      if(d.options.obj) {
        if(d.options.id){
          d.options.obj[d.options.id] = time;
        } else {
          d.options.obj.timeAgo = time;  
        }
      }
    });
  }
  
  dd.timer = w.setInterval(dd.timerCallback, dd.updateInterval);
  dd.timesToCheck = [];
  // must be 2014-08-04T16:09:57.000+04:00 format
  dd.add = function(leDate, options) {
    const d = new Date(leDate);
    dd.timesToCheck.push({date: d, options});
  };
}(window));